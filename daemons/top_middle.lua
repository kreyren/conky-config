conky.config = {
	-- Remove the gaps
	gap_x = 0,
	gap_y = 0,

	-- Define ancor
	alignment = 'top_middle',

	-- TODO(Krey): Decide on the implementation
	minimum_width = 450,

	-- TODO(Krey): To be processed
	background = false,
    border_width = 0.5,
    cpu_avg_samples = 4,
    default_color = 'white',
    default_outline_color = 'grey',
    default_shade_color = 'black',
    draw_borders = true,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    use_xft = true,
    font = 'Unifont:size=16',

    net_avg_samples = 2,
    double_buffer = true,
    out_to_console = false,
    out_to_stderr = false,
    extra_newline = false,
    own_window = true,
    own_window_colour = '000000',
    own_window_class = 'Conky',
    own_window_argb_visual = true,
    own_window_type = 'dock',
    own_window_transparent = true,
    own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
    stippled_borders = 0,
    update_interval = 1,
    uppercase = false,
    show_graph_scale = false,
    show_graph_range = false,
}

conky.text = [[
${voffset 1}
${alignc}-- ${exec date -u +"%H:%M:%S %Z"} --
${alignc}${font :bold:size=32}${execi 3600 date -u +"%d/%m/%Y"}${font}
${alignc}-- ${exec date +"%H:%M:%S %Z"} --
]]
