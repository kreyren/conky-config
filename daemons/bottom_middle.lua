conky.config = {
	-- Remove the gaps
	gap_x = 0,
	gap_y = 20,

	-- Define ancor
	alignment = 'bottom_middle',

	-- TODO(Krey): Decide on the implementation
	minimum_width = 500,

	-- TODO(Krey): To be processed
	background = false,
    border_width = 0.5,
    cpu_avg_samples = 4,
    default_color = 'white',
    default_outline_color = 'grey',
    default_shade_color = 'black',
    draw_borders = true,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    use_xft = true,
    font = 'Unifont:size=16',

    net_avg_samples = 2,
    double_buffer = true,
    out_to_console = false,
    out_to_stderr = false,
    extra_newline = false,
    own_window = true,
    own_window_colour = '000000',
    own_window_class = 'Conky',
    own_window_argb_visual = true,
    own_window_type = 'dock',
    own_window_transparent = true,
    own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
    stippled_borders = 0,
    update_interval = 0.5,
    uppercase = false,
    show_graph_scale = false,
    show_graph_range = false,
}

-- |-${alignc}X${alignr}-|

conky.text = [[
${execi 1 ps -Ao "priority,comm,pid,pcpu,size" --sort=-pcpu | sed -n 1,10p}
]]

-- conky.text = [[
-- ${offset 20}	${alignc}Procs: $processes Run: $running_processes
-- ${offset 20}Name		${alignr}PID   CPU%   MEM%
-- 
-- ${alignc}${top name 1}[${top pid 1}](${top cpu 1}%)${top mem 1}${color}
-- 
-- ${offset 20}${top name 2}	${alignr}${top pid 2} ${top cpu 2} ${top mem 2}
-- ${offset 20}${color red}${top name 3}	${alignr}${top pid 3} ${top cpu 3} ${top mem 3}${color}
-- ${offset 20}${top name 4}	${alignr}${top pid 4} ${top cpu 4} ${top mem 4}
-- ${offset 20}${color red}${top name 5}	${alignr}${top pid 5} ${top cpu 5} ${top mem 5}${color}
-- ${offset 20}${top name 6}	${alignr}${top pid 6} ${top cpu 6} ${top mem 6}
-- ${offset 20}${color red}${top name 7}	${alignr}${top pid 7} ${top cpu 7} ${top mem 7}${color}
-- ${offset 20}${top name 8}	${alignr}${top pid 8} ${top cpu 8} ${top mem 8}
-- ${offset 20}${color red}${top name 9}	${alignr}${top pid 9} ${top cpu 9} ${top mem 9}${color}
-- ${offset 20}${top name 10}	${alignr}${top pid 10} ${top cpu 10} ${top mem 10}
-- ]]
