conky.config = {
	-- Remove the gaps
	gap_x = 10,
	gap_y = 50,

	-- Define ancor
	alignment = 'top_right',

	-- TODO(Krey): Decide on the implementation
	minimum_width = 250,
	-- maximum_width = 400,

	-- TODO(Krey): To be processed
	background = false,
    border_width = 0.5,
    cpu_avg_samples = 4,
    default_color = 'white',
    default_outline_color = 'grey',
    default_shade_color = 'black',
    draw_borders = true,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    use_xft = true,
    font = 'Unifont:size=12',

    net_avg_samples = 2,
    double_buffer = true,
    out_to_console = false,
    out_to_stderr = false,
    extra_newline = false,
    own_window = true,
    own_window_colour = '000000',
    own_window_class = 'Conky',
    own_window_argb_visual = true,
    own_window_type = 'dock',
    own_window_transparent = true,
    own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
    stippled_borders = 0,
    update_interval = 0.1,
    uppercase = false,
    show_graph_scale = false,
    show_graph_range = false,
}

-- |-${alignc}x${alignr}-|

conky.text = [[
${alignc}${font :bold:size=34}${execi 3600 hostname | tr [:lower:] [:upper:]}${font}
${alignc}-- ${execi 3600 "hostname --domain"} --

${font Entopia:bold:size=18}SYSTEM ${font}${hr 2}
${offset 20}Host: ${alignr}${execi 3600 cat /sys/devices/virtual/dmi/id/product_name | sed s/\ //g}
${offset 20}OS: ${alignr}${execi 3600 grep PRETTY_NAME /etc/os-release | sed s/PRETTY_NAME=//g | sed s/\"//g}
${offset 20}Kernel: ${alignr}${sysname} ${kernel}
${offset 20}Architecture: ${alignr}${machine}
${offset 20}Process scheduler: ${alignr}Unknown
${offset 20}Uptime: ${alignr}${uptime}

${font Entopia:bold:size=18}PERFORMANCE ${font}${hr 2}
${offset 20}${font :bold}CPU:${font} i7-2600K ${freq_g} Ghz (${running_threads}/${threads}) ${cpubar cpu0 13}
${offset 30}0: (${execi 1 sensors | grep "Core 0" | cut -c 17-20} C): [${execi 10 cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor}] ${cpubar cpu0 12}
${offset 30}1: (${execi 1 sensors | grep "Core 0" | cut -c 17-20} C): [${execi 10 cat /sys/devices/system/cpu/cpu1/cpufreq/scaling_governor}] ${cpubar cpu1 12}
${offset 30}2: (${execi 1 sensors | grep "Core 1" | cut -c 17-20} C): [${execi 10 cat /sys/devices/system/cpu/cpu2/cpufreq/scaling_governor}] ${cpubar cpu2 12}
${offset 30}3: (${execi 1 sensors | grep "Core 1" | cut -c 17-20} C): [${execi 10 cat /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor}] ${cpubar cpu3 12}
${offset 30}4: (${execi 1 sensors | grep "Core 2" | cut -c 17-20} C): [${execi 10 cat /sys/devices/system/cpu/cpu4/cpufreq/scaling_governor}] ${cpubar cpu4 12}
${offset 30}5: (${execi 1 sensors | grep "Core 2" | cut -c 17-20} C): [${execi 10 cat /sys/devices/system/cpu/cpu5/cpufreq/scaling_governor}] ${cpubar cpu5 12}
${offset 30}6: (${execi 1 sensors | grep "Core 3" | cut -c 17-20} C): [${execi 10 cat /sys/devices/system/cpu/cpu6/cpufreq/scaling_governor}] ${cpubar cpu6 12}
${offset 30}7: (${execi 1 sensors | grep "Core 3" | cut -c 17-20} C): [${execi 10 cat /sys/devices/system/cpu/cpu7/cpufreq/scaling_governor}] ${cpubar cpu7 12}
${offset 30}${loadgraph 80,350 00FF00 FF0000 10 -t 75}
${offset 30}RAM: ${mem} / ${memmax} (${memperc}%)  ${membar 12}
${offset 30}${memgraph 50,350 00FF00 FF0000 -t 75}
${offset 20}${font :size=14}iGPU:${font} Intel HD Graphics 3000
${offset 20}${font :size=14}dGPU:${font} ${font Unifont:size=14:bold}${execi 30 glxinfo | grep Device | sed -E "s#(\s+Device:\s+)([a-zA-Z\ 0-9]+)\s+\((\w+).*#\2#"}${font}
${offset 20} = Architecture: ${execi 30 glxinfo | grep Device | sed -E "s#(\s+Device:\s+)([a-zA-Z\ 0-9]+)\s+\((\w+),\s+([A-Z\ \.]+)([0-9\.]+),\s+[0-9\.]+,\s+[A-Z]+\s+[0-9\.]+\)\s+\([0-9a-z]+\)#\3#"}
${offset 20} = DRM: ${execi 30 glxinfo | grep Device | sed -E "s#(\s+Device:\s+)([a-zA-Z\ 0-9]+)\s+\((\w+),\s+([A-Z\ \.]+)([0-9\.]+).*#\5#"}
${offset 20} = LLVM: ${execi 30 glxinfo | grep Device | sed -E "s#(\s+Device:\s+)([a-zA-Z\ 0-9]+)\s+\((\w+),\s+([A-Z\ \.]+)([0-9\.]+),\s+([0-9\.]+),\s+([A-Z]+)\s+([0-9\.]+)\)\s+\([0-9a-z]+\)#\8#"}
${offset 20} = VBIOS version: ${execi 30 cat /sys/class/drm/card0/device/vbios_version}
${offset 20} = Max Link Speed: ${execi 30 cat /sys/class/drm/card0/device/max_link_speed}
${offset 20} = Voltage: ${execi 1 sensors | grep amdgpu-pci-0700 -A 4 | grep vddgfx | sed -E "s#(\w+):\s+##g"}
${offset 20} = Wattage: ${execi 1 sensors | grep amdgpu-pci-0700 -A 5 | grep PPT | sed -E "s#(\w+:\s+)(\w+\.\w+)\s(\w)\s+\(cap\s\=\s(\w+\.\w+)\s\w\)#\2/\4 \3#"}
${offset 20} = Temperature: ${execi 1 sensors | grep amdgpu-pci-0700 -A 4 | grep edge | sed -E "s#edge:\s+\+(\w+\.\w+)(°C)\s+\(crit\s=\s\+(\w+\.\w+)°C.*#\1\/\3 \2#"}
${offset 20} = FanSpeed: ${execi 1 sensors | grep amdgpu-pci-0700 -A 4 | grep fan1 | sed -E "s#fan1:\s+(\w+)\sRPM\s+\(min\s\=\s+\w+\sRPM\,\smax\s=\s(\w+)\s(RPM)\)#\1/\2 \3#"}
${offset 20} = State: ${execi 1 cat /sys/class/drm/card0/device/power_dpm_state}
${offset 20} = Utilization: ${execi 0.1 cat /sys/class/drm/card0/device/gpu_busy_percent} %
${offset 30}${execigraph 0.1 "cat /sys/class/drm/card0/device/gpu_busy_percent" 80,350 00FF00 FF0000 100 -t 75}
${offset 20} VRAM: ${execi 30 cat /sys/class/drm/card0/device/mem_info_vram_vendor} | ${execi 1 "bc <<< $(cat /sys/class/drm/card0/device/mem_info_vram_used)/1000000"}/${execi 1 "bc <<< $(cat /sys/class/drm/card0/device/mem_info_vram_total)/1000000"} MB
${offset 20} = VRAM Utilization: ${execi 0.1 cat /sys/class/drm/card0/device/mem_busy_percent}
${offset 20}${execigraph 0,1 "cat /sys/class/drm/card0/device/mem_busy_percent" 60,350 00FF00 FF0000 100 -t 75}
${offset 20}Entropy: ${entropy_avail}/${entropy_poolsize} (${entropy_perc}%) ${alignr}${entropy_bar 5,100}

${font Entopia:bold:size=18}DISKS ${hr 2}${font}
${offset 20}${font Unifont:size=16:bold}/dev/sda:${font} [${ioscheduler sda}]
${offset 30}${fs_used /}/${fs_size /} ${fs_bar 8 /}
${offset 30}${diskiograph_read /dev/sda 25,100} ${alignr}${diskiograph_read /dev/sda 25,100}
${offset 30}Read ${diskio_read /dev/sda} ${alignr}Write: ${diskio_write /dev/sda}
${offset 20}${font Unifont:size=16:bold}/dev/sdb:${font} [${ioscheduler sdb}]
${offset 30}${fs_used /home/kreyren}/${fs_size /home/kreyren} ${fs_bar 8 /home/kreyren}
${offset 30}${diskiograph_read /dev/sdb 25,100} ${alignr}${diskiograph_read /dev/sdb 25,100}
${offset 30}Read ${diskio_read /dev/sdb} ${alignr}Write: ${diskio_write /dev/sdb}
${offset 20}Swap: $swap/$swapmax $swapperc% ${swapbar 4}
]]
