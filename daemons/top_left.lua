conky.config = {
	-- Remove the gaps
	gap_x = 0,
	gap_y = 50,

	-- Define ancor
	alignment = 'top_left',

	-- TODO(Krey): Decide on the implementation
	minimum_width = 450,

	-- TODO(Krey): To be processed
	background = false,
    border_width = 0.5,
    cpu_avg_samples = 4,
    default_color = 'white',
    default_outline_color = 'grey',
    default_shade_color = 'black',
    draw_borders = true,
    draw_graph_borders = true,
    draw_outline = false,
    draw_shades = false,
    use_xft = true,
    font = 'Unifont:size=16',

    net_avg_samples = 2,
    double_buffer = true,
    out_to_console = false,
    out_to_stderr = false,
    extra_newline = false,
    own_window = true,
    own_window_colour = '000000',
    own_window_class = 'Conky',
    own_window_argb_visual = true,
    own_window_type = 'dock',
    own_window_transparent = true,
    own_window_hints = 'undecorated,below,sticky,skip_taskbar,skip_pager',
    stippled_borders = 0,
    update_interval = 1,
    uppercase = false,
    show_graph_scale = false,
    show_graph_range = false,
}

conky.text = [[
${font Entopia:bold:size=21}NETWORK ${hr 2}${font}
${offset 20}Internet Service Provider:  ${alignr}Vodafone CZ
${offset 20}Nameserver:						${alignr}${nameserver}
${offset 20}enp7s0						${alignr}${addr enp8s0}
${offset 20}${downspeedgraph enp7s0 50,150}			${alignr}${upspeedgraph enp8s0 50,150}
${offset 20}${color green}${font}▼ $color${downspeed enp8s0}	${alignr}${color green}▲ $color${upspeed enp8s0}

${execigraph 5 "cat /home/kreyren/.local/config/conky/data/ping.log | sed '2q;d' | sed -E "s#.*=(\w+[\.\,]\w+)\sms\$#\1#g"" -t 80 -l 8}
PING: ${execi 5 cat /home/kreyren/.local/config/conky/data/ping.log | sed '2q;d' | sed -E "s#.*=(\w+[\.\,]\w+)\sms\$#\1#g"} ms

${execigraph 5 "bc <<< $(echo $(cat /home/kreyren/.local/config/conky/data/speedtest.json | jq --raw-output '"\(.download)"') / 1000000 / 100)"}
DOWN: ${execi 5 bc <<< $(echo $(cat /home/kreyren/.local/config/conky/data/speedtest.json | jq --raw-output '"\(.download)"') / 8000000)} MB/s (${execi 5 bc <<< $(echo $(cat /home/kreyren/.local/config/conky/data/speedtest.json | jq --raw-output '"\(.download)"') / 1000000)} Mb/s)

${execigraph 5 "bc <<< $(echo $(cat /home/kreyren/.local/config/conky/data/speedtest.json | jq --raw-output '"\(.upload)"') / 100000 / 100)"}
UP: ${execi 5 bc <<< $(echo $(cat /home/kreyren/.local/config/conky/data/speedtest.json | jq --raw-output '"\(.upload)"') / 8000000)} MB/s (${execi 5 bc <<< $(echo $(cat /home/kreyren/.local/config/conky/data/speedtest.json | jq --raw-output '"\(.upload)"') / 1000000)} Mb/s)

<Traffic log here>

<Firewall open ports>

<Tracefoute to ISP>

${font Entopia:bold:size=24}SERVICES ${font}${hr 2}
${offset 20}Tor relay:    ${alignr}[UNKNOWN]
]]
