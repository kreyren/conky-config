All rights reserved by Jacob Hrbek <kreyren@fsfe.org> in 14/06/2021 in 17:19:04 UTC

This license currently mimics the terms of GPLv3 <https://www.gnu.org/licenses/gpl-3.0.en.html> while reserving the right for license update in the future.