#!/bin/sh
# shellcheck shell=sh # Written to comply with IEEE Std 1003.1-2017

#@ Written by Jacob Hrbek <kreyren@fsfe.org> in 14/06/2021 17:39:35 UTC under the terms of GPLv3 license <https://www.gnu.org/licenses/gpl-3.0.en.html>

if command -v ${GUIX:-guix}; then
	conky_conkyDir="${conky_conkyDir:-"$HOME/.local/config/conky"}"
else
	conky_conkyDir="${conky_conkyDir:-"$HOME/.config/conky"}"	
fi

while [ "$#" -ne 0 ]; do case "$1" in
		kill) pkill ${CONKY:-"conky"} ;;
		start)
			[ -d "$conky_conkyDir/data" ] || mkdir "$conky_conkyDir/data"
			# NOTE(Krey): I have to do the fucking renaming, bcs pipelines!

			# Capture data from a speedtest
			while true; do	
				# --server 1363 # Doesn't work for some reason with the CLI?
				${SPEEDTEST:-speedtest} --secure --single --json > "$conky_conkyDir/data/speedtest.json.temp"
				mv "$conky_conkyDir/data/speedtest.json.temp" "$conky_conkyDir/data/speedtest.json"
				sleep 300
			done &

			# Capture data from a ping
			while true; do
				# | sed '2q;d' | sed -E "s#\w+\s+bytes\s+from\s+[a-z\.-]+\s+\([0-9.]+\):\s[a-z_=]+\w\s\w+=[0-9]+\stime=([0-9]+\.[0-9])\s+ms#\1#g"
				ping -c 1 vodafone.cz > "$conky_conkyDir/data/ping.log.temp"
				mv "$conky_conkyDir/data/ping.log.temp" "$conky_conkyDir/data/ping.log"
				sleep 60
			done &

			${CONKY:-"conky"} --config="$conky_conkyDir/daemons/top_right.lua" &
			#${CONKY:-"conky"} --config="$conky_conkyDir/daemons/top_middle.lua" &
			${CONKY:-"conky"} --config="$conky_conkyDir/daemons/top_left.lua" &
			${CONKY:-"conky"} --config="$conky_conkyDir/daemons/bottom_middle.lua" &
			# ${CONKY:-"conky"} --config="$conky_conkyDir/daemons/bottom_right.lua" &
			exit 0
esac; shift 1; done

